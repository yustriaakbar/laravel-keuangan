<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

# Aplikasi Pencatatan Keuangan Laravel 8.
Aplikasi pencatatan pengeluaran dan pemasukan keuangan berbasis web dengan Framework Laravel.

-----
## Table of Contents

* [Features](#item1)
* [Installation Guide](#item2)
* [Screenshots](#item3)

-----
<a name="item1"></a>
## Features:
* Home
  * Menampilkan pemasukan, pengeluaran dan saldo saat ini.
* Kategori
  * List Kategori
  * Tambah Kategori
  * Edit Kategori
  * Delete
* Transaksi
  * List Transaksi
  * Tambah Transaksi
  * Edit Transaksi
  * Delete

-----
<a name="item2"></a>
## Installation Guide:

Clone this repository and install the dependencies.

    $ git clone
    $ cd folder-lokasi-project
    $ composer install

Setup database dengan cara copy file .env.example dan rename menjadi .env. Isikan database pada file .env. 

    $ copy .env.example .env

Selanjutnya lakukan migration agar tabel masuk ke mysql dan setelah itu jangan lupa lakukan php artisan db:seed.
    
    $ php artisan migrate
    $ php artisan db:seed
    $ php artisan key:generate

Finally, serve the application.

    $ php artisan serve

Open [http://localhost:8000](http://localhost:8000) from your browser. 

-----

<a name="item3"></a>
## Screenshots
![home](/uploads/9296a62f67b54890f98bc10417e1f5d3/home.png)

![categories](/uploads/c0f7ba7a1f2b40428e221a1808bcb5e0/categories.png)

![create_category](/uploads/74789fce33b4ba93c303214a978023a5/create_category.png)

![edit_category](/uploads/2aede4e29ae08aa5e67e80896e785938/edit_category.png)

![transaction](/uploads/2f277c0e08f936e8177aa9f56c8d452a/transaction.png)

![edit_transacction](/uploads/ba8c08d5c7b64e6f5176d470e2d2ab6e/edit_transacction.png)
