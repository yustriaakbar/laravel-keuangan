<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\TransactionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/
Route::get('/', [HomeController::class, 'index']);

Route::get('categories', [CategoryController::class, 'index'])->name('categories');
Route::get('new-category', [CategoryController::class, 'create']);
Route::post('/create-category', [CategoryController::class, 'store'])->name('create-category');
Route::get('/edit-category/{id}', [CategoryController::class, 'edit'])->name('edit-category');
Route::post('/update-category/{id}', [CategoryController::class, 'update'])->name('update-category');
Route::get('/delete-category/{id}', [CategoryController::class, 'destroy'])->name('delete-category');

Route::get('transactions', [TransactionController::class, 'index'])->name('transactions');
Route::get('new-transaction', [TransactionController::class, 'create']);
Route::post('/create-transaction', [TransactionController::class, 'store'])->name('create-transaction');
Route::get('/edit-transaction/{id}', [TransactionController::class, 'edit'])->name('edit-transaction');
Route::post('/update-transaction/{id}', [TransactionController::class, 'update'])->name('update-transaction');
Route::get('/delete-transaction/{id}', [TransactionController::class, 'destroy'])->name('delete-transaction');
Route::get('laporan/filter', [TransactionController::class, 'filter']);

Route::get('/list-category/{idtr}', [TransactionController::class, 'list_category']);