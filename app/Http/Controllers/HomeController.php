<?php
  
namespace App\Http\Controllers;
use App\Models\Transaction;
use Illuminate\Http\Request;
  
class HomeController extends Controller
{
    public function index()
    {
		$pemasukan = Transaction::where('type_id', 1)->sum('amount');
		$pengeluaran = Transaction::where('type_id', 2)->sum('amount');
		$saldo = $pemasukan - $pengeluaran;
        return view('home', compact('pemasukan', 'pengeluaran', 'saldo'));
    }
}