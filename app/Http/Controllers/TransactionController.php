<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaction;
use App\Models\Type;
use App\Models\Category;
use Illuminate\Database\Eloquent\Model;
use DB;
use Carbon\Carbon;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = Transaction::whereMonth('updated_at' , Carbon::today()->month)->get();
        $pemasukan = Transaction::where('type_id', 1)->sum('amount');
        $pengeluaran = Transaction::where('type_id', 2)->sum('amount');
        $saldo = $pemasukan - $pengeluaran;
        return view('transaction.index', compact('transactions', 'saldo'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::where('type_id', 1)->get();
        $type = Type::all();

        return view('transaction.create', compact('category', 'type'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'amount' => 'required',
            'description' => 'required',
        ]);
        
        Transaction::create([
            'type_id' => $request->type,
            'category_id' => $request->category,
            'amount' => $request->amount,
            'description' => $request->description,
        ]);
    
        return redirect('/transactions');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $transaction = Transaction::find($id);
        $category = DB::table('categories as ctg')
            ->join('types as tp', 'ctg.type_id', '=', 'tp.id')
            ->where('tp.id', $transaction->type_id)
            ->select('ctg.name as name', 'ctg.id as id' )
            ->get();
        //$category = Category::where('type_id', 1)->get();
        $type = Type::all();

        return view('transaction.edit', compact('transaction', 'category', 'type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $transaction = Transaction::find($id);
            $transaction->type_id = $request->type;
            $transaction->category_id = $request->category;
            $transaction->amount = $request->amount;
            $transaction->description = $request->description;
            $transaction->save();

        return redirect('/transactions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transactions = Transaction::find($id);
        $transactions->delete();
        return redirect('/transactions');
    }

    public function list_category($idtr)
    {
        $category = DB::table('categories as ctg')
            ->join('types as t', 'ctg.type_id', '=', 't.id')
            ->where('t.id', $idtr)
            ->select('ctg.name as name', 'ctg.id as id')
            ->get();

            $output = "";
            foreach($category as $ctg)
            {
                $output .= '<option value ="'.$ctg->id.'">'.$ctg->name.'</option>';
            }

        echo $output;
    }
    
    public function filter(Request $request)
    {
        $tanggal_awal = $request->get('start');
        $tanggal_akhir = $request->get('end');
        $pemasukan = Transaction::where('type_id', 1)->sum('amount');
        $pengeluaran = Transaction::where('type_id', 2)->sum('amount');
        $saldo = $pemasukan - $pengeluaran;
        if ($tanggal_awal != null && $tanggal_akhir != null) 
        {
            $transactions = Transaction::whereBetween('updated_at', [$tanggal_awal, $tanggal_akhir])
                ->get();           
        } 
        return view('transaction.index', compact('transactions', 'saldo'));
    }
}
