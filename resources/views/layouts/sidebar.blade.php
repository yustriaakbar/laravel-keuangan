<div class="list-group">
  <a href="{{ url('/') }}" class="list-group-item list-group-item-action">
    Home
  </a>
  <a href="{{ route('categories') }}" class="list-group-item list-group-item-action @if (Request::is('categories')) active @endif">Categories</a>
  <a href="{{ route('transactions') }}" class="list-group-item list-group-item-action @if (Request::is('transactions')) active @endif">Transactions</a>
</div>
