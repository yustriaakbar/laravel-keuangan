@extends('layouts.app')
@section('judul','Categories')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="container-fluid py-2">
            <div class="row">
                <div class="col-md-2">
                    @include('layouts.sidebar')
                </div>

                <div class="col-md-10">
                    <div class="card">
                        <div class="card-header">Categories</div>
                        
                        <div class="row">
                          <div class="col-md-3">
                          <a href="{{ url('new-category') }}" class="btn btn-secondary ml-3 mt-2">Create Category</a>
                        </div>
                        </div>

                        <div class="card-body">
                          <table class="table">
                            <thead class="thead bg-primary">
                              <tr>
                                <th scope="col">No</th>
                                <th scope="col">Type</th>
                                <th scope="col">Category</th>
                                <th scope="col">Description</th>
                                <th scope="col">Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach($categories as $ctg => $value)
                              <tr>
                                <th scope="row">{{ ++$ctg }}</th>
                                <td>{{ $value->type->name }}</td>
                                <td>{{ $value->name }}</td>
                                <td>{{ $value->description_category }}</td>
                                <td><a href="{{url('edit-category/'.$value->id)}}" class="btn btn-primary mr-2">Edit</a><a delete-id="{{$value->id}}" class="btn btn-danger btn-hapus">Delete</a></td>
                              </tr>
                              @endforeach
                            </tbody>
                          </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
    <script type="text/javascript">
        $(window).on('load', function () {
            $('#modal-notification').modal('show');
        });
    </script>
<script type="text/javascript">
  
        {{--$(document).ready(function () {--}}
        {{--    $('.btn-hapus').click(function (e) {--}}
        {{--        e.preventDefault();--}}
        {{--        var id = $(this).attr('delete-id');--}}
        {{--        var url = "{{ url('delete-category') }}" + '/' + id;--}}
        {{--        $('#modal-notification').find('form').attr('action', url);--}}
        {{--        $('#modal-notification').modal();--}}
        {{--    })--}}
        {{--})--}}
            $('.btn-hapus').click(function (e) {
                var r = confirm("Are you sure you want to delete category ?");
                var id = $(this).attr('delete-id');
                if(r== true)
                {
                    window.location.href = "{{ url('delete-category') }}" + '/' + id;
                } else{
                }
            })
</script>
@endsection
