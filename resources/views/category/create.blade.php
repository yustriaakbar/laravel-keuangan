@extends('layouts.app')
@section('judul','Create Category')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="container-fluid py-2">
            <div class="row">
                <div class="col-md-3">
                    @include('layouts.sidebar')
                </div>

                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">Create Category</div>

                        <div class="card-body">
                          <form method="post" action="{{ url('create-category') }}" enctype="multipart/form-data">

                            {{ csrf_field() }}
                            
                            <div class="form-group">
                              <label>Select Pemasukan/Pengeluaran</label>
                              <select  name="type_id" class="form-control">
                                <option value="">Select Type</option>
                                @foreach ($types as $data)
                                <option value="{{$data->id}}">
                                  {{$data->name}}
                                </option>
                                @endforeach
                              </select>
                            </div>

                            <div class="form-group">
                              <label>Category Name</label>
                              <input type="text" name="name" class="form-control" placeholder="Category Name">

                              @if($errors->has('name'))
                              <div class="text-danger">
                                {{ $errors->first('name')}}
                              </div>
                              @endif

                            </div>

                            <div class="form-group">
                              <label>Category Name</label>
                              <input type="text" name="desc" class="form-control" placeholder="Description Category">

                              @if($errors->has('desc'))
                              <div class="text-danger">
                                {{ $errors->first('desc')}}
                              </div>
                              @endif

                            </div>

                            <div class="form-group">
                              <a href="{{ url('categories') }}" class="btn btn-secondary">Kembali</a>
                              <input type="submit" class="btn btn-success" value="Simpan">
                            </div>

                          </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
