@extends('layouts.app')
@section('judul','Create Transaction')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="container-fluid py-2">
            <div class="row">
                <div class="col-md-3">
                    @include('layouts.sidebar')
                </div>

                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">Create Transaction</div>

                        <div class="card-body">
                          <form method="post" action="{{ url('create-transaction') }}">

                             @csrf

                            <div class="form-group">
                              <label>Pemasukan / Pengeluaran</label>
                              <select  id="daftar_type" name="type" class="form-control">
                                @foreach ($type as $keys => $value)
                                <option value="{{$value->id}}">
                                  {{$value->name}}
                                </option>
                                @endforeach
                              </select>
                            </div>

                            <div class="form-group">
                              <label>Select Category</label>
                              <select id="daftar_category" name="category" class="form-control">
                               @foreach($category as $keys => $value)
                               <option value="{{$value->id}}">{{$value->name}}</option>
                               @endforeach
                             </select>
                           </div>

                            <div class="form-group">
                              <label>Amount</label>
                              <input type="number" name="amount" class="form-control" placeholder="Jumlah, isikan angka tanpa titik atau koma">

                              @if($errors->has('amount'))
                              <div class="text-danger">
                                {{ $errors->first('amount')}}
                              </div>
                              @endif

                            </div>

                            <div class="form-group">
                              <label>Description</label>
                              <textarea class="form-control" name="description" placeholder="Description"></textarea>

                              @if($errors->has('description'))
                              <div class="text-danger">
                                {{ $errors->first('description')}}
                              </div>
                              @endif
                            </div>

                            <div class="form-group">
                              <a href="{{ url('transactions') }}" class="btn btn-secondary">Kembali</a>
                              <input type="submit" class="btn btn-success" value="Simpan">
                            </div>

                          </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
    <script type="text/javascript">

        $("#daftar_type").change(function(){

            var id = $(this).val();
            var token = '{{csrf_token()}}'


            $.ajax({
                url: '{{url("/list-category")}}/' + id,
                type: 'GET',
                data: {
                    "id": id,
                    "_token": token,
                },
                dataType:"text",
                success: function(data) {

                    $('#daftar_category').html(data);

                }
            });

        })


    </script>

@endsection