@extends('layouts.app')
@section('judul','Transaction')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="container-fluid py-2">
            <div class="row">
                <div class="col-md-2">
                    @include('layouts.sidebar')
                </div>

                <div class="col-md-10">
                    <div class="card">
                        <div class="card-header">Transaction</div>
                        
                        <div class="row">
                          <div class="col-md-3">
                          <a href="{{ url('new-transaction') }}" class="btn btn-secondary ml-3 mt-2">Create Transaction</a>
                        </div>
                        </div>
                        
                        <form action="{{url('/laporan/filter')}}" method="get">
                        <div class="row ml-2 mt-2">
                            <div class="col-md-3">
                                <div class="form-group">
                                  <label>Start Date</label>
                                  <input type="date" name="start" class="form-control" required>
                              </div>
                            </div>
                                  
                            <div class="col-md-3">
                              <div class="form-group">
                                  <label>End Date</label>
                                  <input type="date" name="end" class="form-control" required>
                              </div>  
                            </div>

                            <div class="col-md-3">
                              <div class="form-group">
                                  <label><br></label>
                                  <input type="submit" class="form-control btn btn-primary" value="Filter">
                              </div>  
                            </div>
                        </div>
                        </form>

                        <div class="card-body">
                          <h6>Saldo saat ini adalah <i>Rp.{{ number_format($saldo) }}</i></h6>
                          <table class="table">
                            <thead class="thead bg-primary">
                              <tr>
                                <th scope="col">No</th>
                                <th scope="col">Type</th>
                                <th scope="col">Category</th>
                                <th scope="col">Amount</th>
                                <th scope="col">Description</th>
                                <th scope="col">Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach($transactions as $trans => $value)
                              <tr>
                                <th scope="row">{{ ++$trans }}</th>
                                <td>{{ $value->type->name }}</td>
                                <td>{{ $value->category->name }}</td>
                                <td>Rp.{{ number_format($value->amount) }}</td>
                                <td>{{ $value->description }}</td>
                                <td><a href="{{url('edit-transaction/'.$value->id)}}" class="btn btn-primary mr-2">Edit</a><a delete-id="{{$value->id}}" class="btn btn-danger btn-hapus">Delete</a></td>
                              </tr>
                              @endforeach
                            </tbody>
                          </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
    <script type="text/javascript">
        $(window).on('load', function () {
            $('#modal-notification').modal('show');
        });
    </script>
<script type="text/javascript">
  
        {{--$(document).ready(function () {--}}
        {{--    $('.btn-hapus').click(function (e) {--}}
        {{--        e.preventDefault();--}}
        {{--        var id = $(this).attr('delete-id');--}}
        {{--        var url = "{{ url('delete-transaction') }}" + '/' + id;--}}
        {{--        $('#modal-notification').find('form').attr('action', url);--}}
        {{--        $('#modal-notification').modal();--}}
        {{--    })--}}
        {{--})--}}
            $('.btn-hapus').click(function (e) {
                var r = confirm("Are you sure you want to delete transaction ?");
                var id = $(this).attr('delete-id');
                if(r== true)
                {
                    window.location.href = "{{ url('delete-transaction') }}" + '/' + id;
                } else{
                }
            })
</script>
@endsection
