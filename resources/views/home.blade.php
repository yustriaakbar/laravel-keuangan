@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="container-fluid py-2">
            <div class="row">
                <div class="col-md-2">
                    @include('layouts.sidebar')
                </div>
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="card">
                              <div class="card-body">
                                <h5 class="card-title">Saldo Saat Ini</h5>
                                <h4 class="card-text"><b>Rp.{{ number_format($saldo) }}</b></h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="card">
                          <div class="card-body">
                            <h5 class="card-title">Total pengeluaran</h5>
                            <h4 class="card-text"><b>Rp.{{ number_format($pengeluaran) }}</b></h4>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card">
                      <div class="card-body">
                        <h5 class="card-title">Total Pemasukan</h5>
                        <h4 class="card-text"><b>Rp.{{ number_format($pemasukan) }}</b></h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
@endsection
