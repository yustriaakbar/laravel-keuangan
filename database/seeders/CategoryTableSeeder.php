<?php

namespace Database\Seeders;
use DB;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
        	'type_id' => 1,
        	'name' => 'Gaji',
        	'description_category' => 'Gaji Bulanan',
        ]);
        DB::table('categories')->insert([
        	'type_id' => 1,
        	'name' => 'Tunjangan',
        	'description_category' => 'Tunjangan Bulanan',
        ]);
        DB::table('categories')->insert([
        	'type_id' => 1,
        	'name' => 'Bonus',
        	'description_category' => 'Bonus Bulanan',
        ]);
        DB::table('categories')->insert([
        	'type_id' => 2,
        	'name' => 'Sewa Kos',
        	'description_category' => 'Kos Bulanan',
        ]);
        DB::table('categories')->insert([
        	'type_id' => 2,
        	'name' => 'Makan',
        	'description_category' => 'Uang Makan',
        ]);
        DB::table('categories')->insert([
        	'type_id' => 2,
        	'name' => 'Nonton bioskop',
        	'description_category' => 'Nonton Bioskop',
        ]);
    }
}
