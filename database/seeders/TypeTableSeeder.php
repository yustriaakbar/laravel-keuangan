<?php

namespace Database\Seeders;
use DB;
use Illuminate\Database\Seeder;

class TypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types')->insert([
        	'name' => 'Pemasukan',
        ]);
        DB::table('types')->insert([
        	'name' => 'Pengeluaran',
        ]);
    }
}
