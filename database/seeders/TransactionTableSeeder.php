<?php

namespace Database\Seeders;
use DB;
use Illuminate\Database\Seeder;

class TransactionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('transactions')->insert([
        	'type_id' => 1,
        	'category_id' => 1,
        	'amount' => 4000000,
        	'description' => 'Gaji Bulanan Agustus',
        ]);
        DB::table('transactions')->insert([
        	'type_id' => 1,
        	'category_id' => 2,
        	'amount' => 300000,
        	'description' => 'Tunjangan Bulanan Agustus',
        ]);
        DB::table('transactions')->insert([
        	'type_id' => 1,
        	'category_id' => 3,
        	'amount' => 500000,
        	'description' => 'Bonus Bulan Agustus',
        ]);
        DB::table('transactions')->insert([
        	'type_id' => 2,
        	'category_id' => 4,
        	'amount' => 450000,
        	'description' => 'Sewa Kos Bulan Agustus',
        ]);
    }
}
